# --- Created by Slick DDL
# To stop Slick DDL generation, remove this comment and start using Evolutions

# --- !Ups

create table "CLIENTS" ("ID" SERIAL NOT NULL PRIMARY KEY,"USER_ID" INTEGER NOT NULL);
create table "OFFERS" ("ID" SERIAL NOT NULL PRIMARY KEY,"PARTNER_ID" INTEGER,"TYPE_ID" INTEGER NOT NULL,"COST" INTEGER,"time_begin" DATE,"time_end" DATE,"daysOfWeekMask" BIGINT,"guestsCount" INTEGER,"latitude" DOUBLE PRECISION,"longitude" DOUBLE PRECISION,"NAME" VARCHAR(254) NOT NULL,"DESCR" VARCHAR(254),"KEYWORDS" VARCHAR(254),"RATING" DOUBLE PRECISION NOT NULL);
create table "ORDERS" ("ID" SERIAL NOT NULL PRIMARY KEY,"CLIENT_ID" INTEGER NOT NULL,"STATUS" VARCHAR(254) NOT NULL);
create table "ORDER_PARTNERS" ("ORDER_ID" INTEGER NOT NULL,"PARTNER_ID" INTEGER NOT NULL);
create table "PARTNERS" ("ID" SERIAL NOT NULL PRIMARY KEY,"USER_ID" INTEGER NOT NULL,"VERIFIED" BOOLEAN NOT NULL);
create table "TYPES" ("ID" SERIAL NOT NULL PRIMARY KEY,"PARENT_TYPE_ID" INTEGER NOT NULL,"DESCR" VARCHAR(254) NOT NULL);
create table "USERS" ("ID" SERIAL NOT NULL PRIMARY KEY,"NAME" VARCHAR(254) NOT NULL,"EMAIL" VARCHAR(254),"EMAIL_PASS_SHA" VARCHAR(254),"VK_ID" INTEGER,"GITHUB_ID" INTEGER,"ACCESS_TOKEN" VARCHAR(254));
create unique index "unique_github_id" on "USERS" ("GITHUB_ID");
create unique index "unique_vk_id" on "USERS" ("VK_ID");
alter table "CLIENTS" add constraint "USER_FK" foreign key("USER_ID") references "USERS"("ID") on update NO ACTION on delete NO ACTION;
alter table "OFFERS" add constraint "PART_FK" foreign key("PARTNER_ID") references "PARTNERS"("ID") on update NO ACTION on delete NO ACTION;
alter table "OFFERS" add constraint "TYPE_FK" foreign key("TYPE_ID") references "TYPES"("ID") on update NO ACTION on delete NO ACTION;
alter table "ORDERS" add constraint "CLNT_FK" foreign key("CLIENT_ID") references "USERS"("ID") on update NO ACTION on delete NO ACTION;
alter table "ORDER_PARTNERS" add constraint "OFFER_FK" foreign key("PARTNER_ID") references "OFFERS"("ID") on update NO ACTION on delete NO ACTION;
alter table "ORDER_PARTNERS" add constraint "USER_FK" foreign key("ORDER_ID") references "ORDERS"("ID") on update NO ACTION on delete NO ACTION;
alter table "PARTNERS" add constraint "USER_FK" foreign key("USER_ID") references "USERS"("ID") on update NO ACTION on delete NO ACTION;

# --- !Downs

alter table "PARTNERS" drop constraint "USER_FK";
alter table "ORDER_PARTNERS" drop constraint "OFFER_FK";
alter table "ORDER_PARTNERS" drop constraint "USER_FK";
alter table "ORDERS" drop constraint "CLNT_FK";
alter table "OFFERS" drop constraint "PART_FK";
alter table "OFFERS" drop constraint "TYPE_FK";
alter table "CLIENTS" drop constraint "USER_FK";
drop table "USERS";
drop table "TYPES";
drop table "PARTNERS";
drop table "ORDER_PARTNERS";
drop table "ORDERS";
drop table "OFFERS";
drop table "CLIENTS";

