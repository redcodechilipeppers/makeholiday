
package service

import securesocial.core._
import securesocial.core.services._
import securesocial.core.providers._
import securesocial.core.providers.utils.{ Mailer, PasswordHasher, PasswordValidator }

import scala.collection.immutable.ListMap

class MyEnvironment extends RuntimeEnvironment.Default[DemoUser] {
  override val userService: UserService[DemoUser] = new InMemoryUserService()
  override lazy val providers = ListMap(
    // oauth 2 client providers
    // include(new FacebookProvider(routes, cacheService, oauth2ClientFor(FacebookProvider.Facebook))),
    // include(new FoursquareProvider(routes, cacheService, oauth2ClientFor(FoursquareProvider.Foursquare))),
    include(new GitHubProvider(routes, cacheService, oauth2ClientFor(GitHubProvider.GitHub))),
    // include(new GoogleProvider(routes, cacheService, oauth2ClientFor(GoogleProvider.Google))),
    // include(new InstagramProvider(routes, cacheService, oauth2ClientFor(InstagramProvider.Instagram))),
    // include(new ConcurProvider(routes, cacheService, oauth2ClientFor(ConcurProvider.Concur))),
    // include(new SoundcloudProvider(routes, cacheService, oauth2ClientFor(SoundcloudProvider.Soundcloud))),
    //include(new LinkedInOAuth2Provider(routes, cacheService,oauth2ClientFor(LinkedInOAuth2Provider.LinkedIn))),
    include(new VkProvider(routes, cacheService, oauth2ClientFor(VkProvider.Vk))),
    // include(new DropboxProvider(routes, cacheService, oauth2ClientFor(DropboxProvider.Dropbox))),
    // include(new WeiboProvider(routes, cacheService, oauth2ClientFor(WeiboProvider.Weibo))),
    // include(new ConcurProvider(routes, cacheService, oauth2ClientFor(ConcurProvider.Concur))),
    // oauth 1 client providers
    // include(new LinkedInProvider(routes, cacheService, oauth1ClientFor(LinkedInProvider.LinkedIn))),
    // include(new TwitterProvider(routes, cacheService, oauth1ClientFor(TwitterProvider.Twitter))),
    // include(new XingProvider(routes, cacheService, oauth1ClientFor(XingProvider.Xing))),
    // username password
    include(new UsernamePasswordProvider[DemoUser](userService, avatarService, viewTemplates, passwordHashers))
  )
}