package models

import java.sql.{Date, Timestamp}

import play.api.libs.json._
import play.api.libs.functional.syntax._

case class User(
                 id: Option[Int],
                 name: String,
                 email: Option[String] = None,
                 email_pass_sha: Option[String] = None,
                 vk_id: Option[Int] = None,
                 github_id: Option[Int] = None,
                 access_token: Option[String] = None
                 ){
  lazy val client:Client = id.map(Clients.find_by_user(_).get).get
  lazy val partner:Partner = id.map(Partners.find_by_user(_).get).get
}
object User {

}

case class Partner(
                    id:Option[Int],
                    user_id: Int,
                    isVerified:Boolean = false) {
  lazy val user = Users.find(user_id)
  lazy val orders: List[Order] = id.map(Orders.find_by_partner(_)).getOrElse(List[Order]())
  lazy val specsWithCosts: List[Offer] = id.map(Offers.find_by_partner(_)).getOrElse(List())
}
case class Client(
                   id:Option[Int],
                   user_id: Int) {
  lazy val user = Users.find(user_id)
  lazy val orders:List[Order] = id.map(Orders.find_by_client(_)).getOrElse(List())
  lazy val offers:List[Offer] = id.map(Offers.find_by_client(_)).getOrElse(List())
}
case class Order(
                  id:Option[Int],
                  client_id: Int,
                  state: String = Order.STATUS_INIT) {
  lazy val user:Option[User] = Clients.find(client_id).map(_.user).getOrElse(None)
  lazy val offers:Seq[Offer] = id.map(Offers.find_by_order(_)).getOrElse(Seq())
  //  lazy val partnerSpecs = ???
}
object Order {
  val STATUS_INIT = "INITIAL" // client
  val STATUS_ACCEPTED = "ACCEPTED" // partner if not fantom
  val STATUS_PREPAID = "PREPAID" // p
  val STATUS_PAID = "PAID" // p
  val STATUS_COMPLETED = "COMPLETED" // c
}
case class Specialization(
                           id:Option[Int],
                           parentId:Int,
                           description:String){
  def isRootSpecialization:Boolean = id.map(_ == parentId).getOrElse(false)
}
case class Offer(
                            id:Option[Int],
                            partner_id:Option[Int],
                            specialization_id:Int,
                            cost:Option[Int],
                            time_begin:Option[Date],
                            time_end:Option[Date],
                            daysOfWeekMask:Option[Long], // 1111100 - будние
                            guestsCount:Option[Int],
                            latitude:Option[Double],
                            longitude:Option[Double],
                            name: String = "<noname>",
                            description: Option[String] = None,
                            keywords: Option[String] = None,
                            rating: Double = 1.0) {
  lazy val specialization = Orders.find(specialization_id)
  lazy val partner = partner_id.map( Partners.find(_) )
  lazy val orders = id.map(Orders.find_by_offer(_))
}

case class OrderOffer(order_id:Int, offer_id:Int) {
  lazy val order = Orders.find(order_id)
  lazy val offer = Partners.find(offer_id)
}


object JsonImplicits {

  implicit val userWrites: Writes[User] = (
      (JsPath \ "id").writeNullable[Int] and
      (JsPath \ "name").write[String] and
      (JsPath \ "email").writeNullable[String] and
      (JsPath \ "email_pass_sha").writeNullable[String] and
      (JsPath \ "vk_id").writeNullable[Int] and
      (JsPath \ "github_id").writeNullable[Int] and
      (JsPath \ "access_token").writeNullable[String]
    )(unlift(User.unapply))

  implicit val userReads: Reads[User] = (
    (JsPath \ "id").readNullable[Int] and
      (JsPath \ "name").read[String] and
      (JsPath \ "email").readNullable[String] and
      (JsPath \ "email_pass_sha").readNullable[String] and
      (JsPath \ "vk_id").readNullable[Int] and
      (JsPath \ "github_id").readNullable[Int] and
      (JsPath \ "access_token").readNullable[String]
    )(User.apply _)

  implicit val offerWrites: Writes[Offer] = (
    (JsPath \ "id").writeNullable[Int] and
      (JsPath \ "partner_id").writeNullable[Int] and
      (JsPath \ "spec_id").write[Int] and
      (JsPath \ "cost").writeNullable[Int] and
      (JsPath \ "time_begin").writeNullable[Date] and
      (JsPath \ "time_end").writeNullable[Date] and
      (JsPath \ "daysOfWeekMask").writeNullable[Long] and
      (JsPath \ "guestsCount").writeNullable[Int] and
      (JsPath \ "latitude").writeNullable[Double] and
      (JsPath \ "latitude").writeNullable[Double] and
      (JsPath \ "name").write[String] and
      (JsPath \ "descr").writeNullable[String] and
      (JsPath \ "keywords").writeNullable[String] and
      (JsPath \ "rating").write[Double]
    )(unlift(Offer.unapply))

  implicit val offerReads: Reads[Offer] = (
    (JsPath \ "id").readNullable[Int] and
      (JsPath \ "partner_id").readNullable[Int] and
      (JsPath \ "spec_id").read[Int] and
      (JsPath \ "cost").readNullable[Int] and
      (JsPath \ "time_begin").readNullable[Date] and
      (JsPath \ "time_end").readNullable[Date] and
      (JsPath \ "daysOfWeekMask").readNullable[Long] and
      (JsPath \ "guestsCount").readNullable[Int] and
      (JsPath \ "latitude").readNullable[Double] and
      (JsPath \ "latitude").readNullable[Double] and
      (JsPath \ "name").read[String] and
      (JsPath \ "descr").readNullable[String] and
      (JsPath \ "keywords").readNullable[String] and
      (JsPath \ "rating").read[Double]
    )(Offer.apply _)
//    )(Offer.apply _)

  implicit val orderWrites: Writes[Order] = (
    (__ \ "id").writeNullable[Int] and
      (__ \ "user").writeNullable[User] and
      (__ \ "offers").write[Seq[Offer]] and
      (__ \ "state").write[String]
    )(order => (order.id, order.user, order.offers, order.state))

  implicit val orderReads: Reads[Order] = (
    (__ \ "id").readNullable[Int] and
      (__ \ "user").readNullable[User] and
      (__ \ "state").read[String]
    )((id, user, state) => Order(id, user.map(_.client.id.getOrElse(-1)).getOrElse(-1), state))
}
