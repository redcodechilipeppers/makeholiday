package models

import java.sql.{Date, Timestamp}

import play.libs.Json

import scala.slick.driver.PostgresDriver.simple._
import play.api.Play.current
import DB._
import play.api.data.Forms._


class Users(tag: Tag) extends Table[User](tag, "USERS") {
  def id = column[Int]("ID", O.PrimaryKey, O.AutoInc)
  def name = column[String]("NAME", O.NotNull)
  def email = column[Option[String]]("EMAIL")
  def email_pass_sha = column[Option[String]]("EMAIL_PASS_SHA")
  def vk_id = column[Option[Int]]("VK_ID")
  def github_id = column[Option[Int]]("GITHUB_ID")
  def access_token = column[Option[String]]("ACCESS_TOKEN") // TODO expire in db

  def * = (id.?, name, email, email_pass_sha, vk_id, github_id, access_token) <> ((User.apply _).tupled, User.unapply)

  def uniqueVkId     = index("unique_vk_id", vk_id, unique = true)
  def uniqueGitHubId = index("unique_github_id", github_id, unique = true)
}


class Partners(tag: Tag) extends Table[Partner](tag, "PARTNERS") {
  def id = column[Int]("ID", O.PrimaryKey, O.AutoInc)
  def user_id = column[Int]("USER_ID")
  def verified = column[Boolean]("VERIFIED")

  def * = (id.?, user_id, verified) <> (Partner.tupled, Partner.unapply)

  def user = foreignKey("USER_FK", user_id, users)(_.id)
}

class Offers(tag: Tag) extends Table[Offer](tag, "OFFERS") {
  def id = column[Int]("ID", O.PrimaryKey, O.AutoInc)
  def partner_id = column[Option[Int]]("PARTNER_ID")
  def specialization_id = column[Int]("TYPE_ID")
  def cost_bottom = column[Option[Int]]("COST")
  def time_begin = column[Option[Date]]("time_begin")
  def time_end = column[Option[Date]]("time_end")
  def daysOfWeekMask = column[Option[Long]]("daysOfWeekMask") // 1111100 - будние
  def guestsCount = column[Option[Int]]("guestsCount")
  def latitude = column[Option[Double]]("latitude")
  def longitude = column[Option[Double]]("longitude")
  def name = column[String]("NAME")
  def description = column[Option[String]]("DESCR")
  def keywords = column[Option[String]]("KEYWORDS")
  def rating = column[Double]("RATING")

  def * = (id.?, partner_id, specialization_id, cost_bottom, time_begin, time_end, daysOfWeekMask, guestsCount,
    latitude, longitude, name, description, keywords, rating) <> (Offer.tupled, Offer.unapply)

  def partner = foreignKey("PART_FK", partner_id, partners)(_.id)
  def specialization   = foreignKey("TYPE_FK", specialization_id, specializations)(_.id)
}


class Specializations(tag: Tag) extends Table[Specialization](tag, "TYPES") {
  def id = column[Int]("ID", O.PrimaryKey, O.AutoInc)
  def parent_specialization_id = column[Int]("PARENT_TYPE_ID") // "==" => isParent
  def decsription = column[String]("DESCR")

  def * = (id.?, parent_specialization_id, decsription) <> (Specialization.tupled, Specialization.unapply)
}


class Clients(tag: Tag) extends Table[Client](tag, "CLIENTS") {
  def id = column[Int]("ID", O.PrimaryKey, O.AutoInc)
  def user_id = column[Int]("USER_ID")

  def * = (id.?, user_id) <> (Client.tupled, Client.unapply)

  def user = foreignKey("USER_FK", user_id, users)(_.id)
}

class OrderOffers(tag: Tag) extends Table[OrderOffer](tag, "ORDER_PARTNERS") {
  def order_id = column[Int]("ORDER_ID")
  def offer_id = column[Int]("PARTNER_ID")

  def * = (order_id, offer_id) <> (OrderOffer.tupled, OrderOffer.unapply)

  def order = foreignKey("USER_FK", order_id, DB.orders)(_.id)
  def offers = foreignKey("OFFER_FK", offer_id, DB.offers)(_.id)
}


class Orders(tag: Tag) extends Table[Order](tag, "ORDERS") {
  def id = column[Int]("ID", O.PrimaryKey, O.AutoInc)
  def client_id = column[Int]("CLIENT_ID")
  // def rating = 
  def status = column[String]("STATUS")

  def * = (id.?, client_id, status) <> ((Order.apply _).tupled, Order.unapply)

  def client = foreignKey("CLNT_FK", client_id, users)(_.id)
}

object DB {
  val db = play.api.db.slick.DB
  val users = TableQuery[Users]
  val partners = TableQuery[Partners]
  val offers = TableQuery[Offers]
  val specializations = TableQuery[Specializations]
  val clients = TableQuery[Clients]
  val orders = TableQuery[Orders]
  val orderOffers = TableQuery[OrderOffers]
}




object Users {
  def all() = db.withSession { implicit session =>
    users.sortBy(_.id.asc.nullsFirst).list
  }
  def add(user:User):Int = db.withSession { implicit session =>
    (users returning users.map(_.id)) += user
  }
  def update(user:User) = update_by_id(user)
  private def update_by_id(user:User) = db.withSession { implicit session =>
    users.filter(_.id === user.id).update(user)
  }
  def find(id:Int):Option[User] = db.withSession { implicit session =>
    users.filter(_.id === id).firstOption
  }
  def find_by(provider:String, id:Int):Option[User] = db.withSession { implicit session =>
    provider match {
      case "github" => users.filter(_.github_id === id).firstOption
      case "vk" => users.filter(_.vk_id === id).firstOption
    }
  }
  def find_by_token(token: String):Option[User] = db.withSession { implicit session =>
    users.filter(_.access_token === token).firstOption
  }
  def delete(id:Int) = db.withSession { implicit session =>
    users.filter(_.id === id).delete
  }
}
object Partners {
  def all() = db.withSession { implicit session =>
    partners.sortBy(_.id.asc.nullsFirst).list
  }
  def add(partner:Partner) = db.withSession { implicit session =>
    (partners returning partners.map(_.id)) += partner
  }
  def update(partner:Partner) = db.withSession { implicit session =>
    partners.filter(_.id === partner.id).update(partner)
  }
  def find(id:Int) = db.withSession { implicit session =>
    partners.filter(_.id === id).firstOption
  }
  def find_by_user(user_id: Int) = db.withSession { implicit session =>
    partners.filter(_.user_id === user_id).firstOption
  }
}
object Clients {
  def all() = db.withSession { implicit session =>
    clients.sortBy(_.id.asc.nullsFirst).list
  }
  def add(client:Client) = db.withSession { implicit session =>
    (clients returning clients.map(_.id)) += client
  }
  def update(client:Client) = db.withSession { implicit session =>
    clients.filter(_.id === client.id).update(client)
  }
  def find(id:Int) = db.withSession { implicit session =>
    clients.filter(_.id === id).firstOption
  }
  def find_by_user(user_id: Int) = db.withSession { implicit session =>
    clients.filter(_.user_id === user_id).firstOption
  }
  def delete(id:Int) = db.withSession { implicit session =>
    clients.filter(_.id === id).delete
  }
}
object Orders {
  def all() = db.withSession { implicit session =>
    orders.sortBy(_.id.asc.nullsFirst).list
  }
  def add(order:Order) = db.withSession { implicit session =>
    (orders returning orders.map(_.id)) += order
  }
  def update(order:Order) = db.withSession { implicit session =>
    orders.filter(_.id === order.id).update(order)
  }
  def find(id:Int) = db.withSession { implicit session =>
    orders.filter(_.id === id).firstOption
  }
  def find_by_client(client_id: Int) = db.withSession { implicit session =>
    orders.filter(_.client_id === client_id).list // TODO sort?
  }
  def find_by_offer(offer_id: Int):List[Order] = db.withSession { implicit session =>
    (for {
      orderOffer <- orderOffers.filter(_.offer_id === offer_id)
      ord <- orders if orderOffer.order_id === ord.id
    } yield ord).list
  }
  def find_by_partner(part_id: Int):List[Order] = db.withSession { implicit session =>
    (for {
//      part <- partners if part.id === part_id
      offer <- offers if offer.partner_id === part_id
      orderOffer <- orderOffers if orderOffer.offer_id === offer.id
      order <- orders if orderOffer.order_id === order.id
    } yield order).list
  }
  def delete(id:Int) = db.withSession { implicit session =>
    orders.filter(_.id === id).delete
  }
}
object Specializations {
  def all() = db.withSession { implicit session =>
    specializations
  }
  def add(spec:Specialization) = db.withSession { implicit session =>
    (specializations returning specializations.map(_.id)) += spec
  }
  def update(spec:Specialization) = db.withSession { implicit session =>
    specializations.filter(_.id === spec.id).update(spec)
  }
  def find(id:Int) = db.withSession { implicit session =>
    specializations.filter(_.id === id).firstOption
  }
  def delete(id:Int) = db.withSession { implicit session =>
    specializations.filter(_.id === id).delete
  }
}
object Offers {
  def all() = db.withSession { implicit session =>
    offers.list
  }
  def add(pspec:Offer) = db.withSession { implicit session =>
    (offers returning offers.map(_.id)) += pspec
  }
  def update(pspec:Offer) = db.withSession { implicit session =>
    offers.filter(_.id === pspec.id).update(pspec)
  }
  def find(id:Int) = db.withSession { implicit session =>
    offers.filter(_.id === id).firstOption
  }
  def find_by_partner(partner_id:Int) = db.withSession { implicit session =>
    offers.filter(_.partner_id === partner_id).list
  }
  def find_by_client(client_id:Int):List[Offer] = db.withSession { implicit session =>
    (for {
      order <- orders if order.client_id === client_id
      orderOffer <- orderOffers if orderOffer.order_id === order.id
      offer <- offers if offer.id === orderOffer.offer_id
    } yield offer).list
  }
  def find_by_order(order_id: Int):List[Offer] = db.withSession { implicit session =>
    (for {
      orderOffer <- orderOffers.filter(_.order_id === order_id)
      off <- offers if orderOffer.offer_id === off.id
    } yield off).list
  }
  def delete(id:Int) = db.withSession { implicit session =>
    offers.filter(_.id === id).delete
  }
}