package models

import java.sql.Date

/**
 * Created by avelier on 15.01.15.
 */
object Recom {
  def offers(filters: Map[String,String]):Seq[Offer] = {
    def max_price = filters.get("max_price").map(_.toInt).getOrElse(Int.MaxValue)
    def min_price = filters.get("min_price").map(_.toInt).getOrElse(0)

    // sample
    Seq(
      Offer(Some(1), Some(0), 0, Some(1000),
        Some(new Date(System.currentTimeMillis())), Some(new Date(System.currentTimeMillis() + 10000)),
        Some(7), Some(15), Some(0.0), Some(0.0)),
      Offer(Some(1), Some(1), 0, Some(3000),
        Some(new Date(System.currentTimeMillis())), Some(new Date(System.currentTimeMillis() + 10000)),
        Some(1+2+7+15), Some(10), Some(30.0), Some(-15.0))
    )
      .filter(_.cost.get > min_price)
      .filter(_.cost.get < max_price)
  }

  def orders(filters: Map[String,String]):Seq[Order] = {
//    def max_price = filters.get("max_price").map(_.toInt).getOrElse(Int.MaxValue)
//    def min_price = filters.get("min_price").map(_.toInt).getOrElse(0)

    // sample // may not work for random values, not sure...
    Seq(
      Order(Some(1), 1, Order.STATUS_INIT),
      Order(Some(2), 1, Order.STATUS_PREPAID)
    )
  }
}
