package models

/**
 * Created by avelier on 15.01.15.
 */
class ApiException(val code:Int, val message:String) extends Exception(message)
object NotFoundException extends ApiException(404, "not found")
