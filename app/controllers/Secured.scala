package controllers

import models._
import play.api.mvc._

/**
 * Created by avelier on 17.01.15.
 */
trait Secured {
  
  // TODO in properties file
  val AUTH_ID = "X-ID"
  val AUTH_TOKEN_HEADER = "X-AUTH-TOKEN"

  def userinfo(request: RequestHeader):Option[User] = {
    for {
      id <- request.session.get(AUTH_ID)
      token <- request.session.get(AUTH_TOKEN_HEADER)
      user <- checked(id, token)
    } yield user
  }

  def onUnauthorized(request: RequestHeader) = Results.Redirect(routes.Application.index)// TODO /login

  private def checked(id: String, token: String) = {
    Users.find_by_token(token) match {
      case Some(user) if (s"${user.id.get}" equals id) => Some(user)
      case _ => None
    }
  }

  def withUser(f: => User => Request[AnyContent] => Result) = {
    Security.Authenticated(userinfo, onUnauthorized) { user =>
      Action(request => {
        f(user)(request)
      })
    }
  }
}

object Auth extends Controller {

}