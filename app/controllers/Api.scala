package controllers

import controllers.UsersController._
import play.api.mvc._
import play.api.libs.json._
import play.api.libs.json.Json._

import models._
import models.JsonImplicits._

/**
 * Created by avelier on 14.01.15.
 */
object Api  extends Controller {
  private def api[T] (code:Int, message:String = "ok")(res : => T)(implicit tjs: Writes[T]):Result = {
    try {
      success(code, message, toJson(res))
    } catch {
      case e: ApiException => fail(e.code, e.getMessage)
      case e: Exception => fail(INTERNAL_SERVER_ERROR, e.getMessage)
    }
  }
  def usingJsonBody[R](f: JsValue => R)(implicit request: Request[AnyContent]): R ={
    request.body.asJson.map {
      json => f(json)
    }.getOrElse(
        throw new ApiException(BAD_REQUEST, "Expecting Json data")
      )
  }
//  private def jsonResp[T](res: T, message:String = "ok")(implicit tjs: Writes[T]) = {
//    JsObject(Seq(
//      "message" -> JsString(message),
//      "data" -> toJson(res)
//    ))
//  }
  private def success(code: Int, message: String, js: JsValue) = {
    Status(code) {
      JsObject(Seq(
        "message" -> JsString(message),
        "data" -> js
      ))
    }
  }
  private def fail(code: Int, message: String) = {
    Status(code) {
      JsObject(Seq(
        "message" -> JsString(message)
      ))
    }
  }

  def getUser(id: Int) = Action { implicit request =>
    api(OK) {
      Users.find(id) match {
        case Some(user) => user
        case _ => throw NotFoundException
      }
    }
  }
  def postUser(name: String) = Action { implicit request =>
    api(CREATED) {
      Users.add(User(None, name))
    }
  }
  def echoJson() = Action { implicit request =>
    api(OK) {
      request.body.asJson.map { json =>
        prettyPrint(json)
      }.getOrElse(
          throw new ApiException(BAD_REQUEST, "Expecting Json data")
        )
    }
  }

  def getOrders() = Action { implicit request =>
    api(OK) {
      Recom.orders(request.queryString.map { case (k, v) => k -> v.mkString})
    }
  }
  def getOffers() = Action { implicit request =>
    api(OK) {
      Recom.offers(request.queryString.map { case (k, v) => k -> v.mkString})
    }
  }
  def postOffer() = withUser {user => implicit request =>
    api(OK){
      usingJsonBody{ json =>
        // order_id, offer
        def ord_id = (json \ "order_id").asOpt[Int]
        if (ord_id.nonEmpty)
          if( !user.client.orders.map(_.id).contains(ord_id.get) )
            throw new ApiException(FORBIDDEN, "This user not own this order")
        val ins_order_id:Int = Orders.add(Order(ord_id, user.client.id.get))

        val ins_offer_id = fromJson[Offer](json \ "offer") match {
          case JsSuccess(offer:Offer, _) => Offers.add(offer)
          case JsError(_) => throw new ApiException(BAD_REQUEST, "Wrong Offer format")
        }
        Json.obj(
          "order_id" -> ins_order_id,
          "offer_id" -> ins_offer_id
        )
      }
    }
  }
  def delOffer(id: Int) = withUser {user => implicit request =>
    api(OK){
      if (!user.client.offers.map(_.id.get).contains(id))
        throw new ApiException(FORBIDDEN, "Can't remove other user offers")
      Offers.delete(id) match {
        case 1 => "deleted"
        case 0 => throw NotFoundException
        case _ => throw new ApiException(200, "Deleted too much o_O")
      }
    }
  }
}
