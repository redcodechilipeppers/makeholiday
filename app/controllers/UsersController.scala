package controllers

import scala.concurrent._
import play.api.libs.concurrent.Execution.Implicits.defaultContext

import play.api._
import play.api.mvc._
import play.api.data._
import play.api.data.Forms._
import play.api.Play.current
import scala.slick.driver.PostgresDriver.simple._
import models.User
import models.Users

object UsersController extends Controller with Secured {
	val userForm = Form(
	    mapping(
          "id" -> optional(number),
	        "name" -> text,
          "email" -> optional(text),
          "email_pass" -> optional(text),
	        "vk_id" -> optional(number),
	        "github_id" -> optional(number),
	        "access_token" -> optional(text)
	    )(User.apply)(User.unapply))
  
 // def index = Action {
 //   Ok(views.html.index(Users.all))
 // }
	def show(id:Int) = Action {
    Users.find(id) match {
      case None => NotFound("404")
      case Some(user) => Ok(views.html.users.show(user))
    }
	}
	def add = Action {
	    Ok(views.html.users.add(userForm))
	}
	def save = Action{implicit request =>
		val user = userForm.bindFromRequest.get
		Users.add(user)
		Redirect(routes.Application.index)
	}
	def edit(id:Int) = Action {
    Users.find(id) match {
      case None => NotFound("404")
      case Some(user) => Ok(views.html.users.edit(id, userForm.fill(user)))
    }
	}
	def update(updateid: Int) = Action {implicit request =>
		val user = userForm.bindFromRequest.get
		val newuser = user.copy(id = Some(updateid))
		Users.update(newuser)
		Redirect(routes.Application.index)
	}
	def delete(id: Int) = withUser {user => implicit request =>
		Users.delete(id)
		Redirect(routes.Application.index)
	}
}
